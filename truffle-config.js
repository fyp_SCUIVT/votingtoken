require('dotenv').config();
require('babel-register');
require('babel-polyfill');
const WalletProvider = require("truffle-wallet-provider");
const ProviderEngine = require("web3-provider-engine");
const Wallet = require('ethereumjs-wallet');
const Web3 = require("web3");
const w3 = new Web3();

const PRIVKEY = "048C30C5FF97006B0AC3AC216F1E0C31CEF078D13E600C56AB9E6621F0E26F86"
module.exports = {

  networks: {
   
    rinkeby:{
      host: "localhost",
     provider: function() {
        return new WalletProvider(
          Wallet.fromPrivateKey(
            Buffer.from(PRIVKEY, "hex")),"https://rinkeby.infura.io/v3/bc210b4e84e24ba2a327428c94c5da20"
        );
        },
      gasPrice:10000000000,
      network_id:4,
    },
     development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*",
    },
  }
};