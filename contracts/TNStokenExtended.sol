pragma solidity ^0.5.0;
import "openzeppelin-solidity/contracts/token/ERC721/ERC721.sol";
contract TNStokenExtended is ERC721{

 string public name;
    string public symbol;
    uint8 public decimals;
    uint256 totalSupply_;
  constructor() public {
  name = "The Neverending Story Token";
        symbol = "TNS";
        decimals = 18;
        totalSupply_ = 100 * 10**6 * 10**18;
  }
}
